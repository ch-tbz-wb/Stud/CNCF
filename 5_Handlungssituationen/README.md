# Handlungsziele und Handlungssituationen 

### **1. Kubernetes in the Enterprise**

Der Studierende ist in der Lage, Kubernetes in einer Unternehmensumgebung zu implementieren und zu verwalten, einschliesslich der Integration von Sicherheitsrichtlinien, Skalierung und Überwachung.

**Typische Handlungssituation:**  
Ein Unternehmen möchte Kubernetes in seiner IT-Infrastruktur einsetzen, um Workloads effizient zu orchestrieren und zu skalieren. Der Studierende wird beauftragt, Kubernetes in einer Produktionsumgebung zu implementieren und dabei Aspekte wie Sicherheitsrichtlinien, Netzwerkisolierung und Skalierbarkeit zu berücksichtigen. Das Ziel ist es, Kubernetes nahtlos in die bestehende IT-Infrastruktur des Unternehmens zu integrieren.

---

### **2. OpenShift**

Der Studierende kann grundlegende Funktionen von OpenShift verstehen und erste einfache Anwendungen darin bereitstellen.

**Typische Handlungssituation:**  
Ein Unternehmen plant, OpenShift als Kubernetes-Distribution zu testen. Der Studierende wird beauftragt, eine einfache OpenShift-Umgebung zu installieren und eine Beispielanwendung darauf bereitzustellen. Dabei soll er erste Erfahrungen mit den integrierten DevOps-Tools und den Grundfunktionen der Plattform sammeln.

---

### **3. VMware Tanzu**

Der Studierende kann grundlegende Konzepte von VMware Tanzu verstehen und erste Schritte zur Bereitstellung eines Kubernetes-Clusters ausführen.

**Typische Handlungssituation:**  
Ein Unternehmen plant, VMware Tanzu zu nutzen, um Kubernetes-Cluster auf einer VMware-Infrastruktur bereitzustellen. Der Studierende wird beauftragt, einen einfachen Cluster mit VMware Tanzu zu erstellen, erste Konfigurationen vorzunehmen und grundlegende Funktionen der Plattform kennenzulernen.

---

### **4. Azure Kubernetes Cluster**

Der Studierende versteht die Grundlagen des Azure Kubernetes Service (AKS) und kann einen einfachen Cluster in der Azure-Umgebung bereitstellen.

**Typische Handlungssituation:**  
Ein Unternehmen möchte Kubernetes-Workloads auf Microsoft Azure bereitstellen. Der Studierende wird beauftragt, einen grundlegenden **AKS** Cluster zu erstellen und einfache Anwendungen zu deployen. Dabei soll er erste Erfahrungen mit der Verbindung von Azure-Ressourcen wie Netzwerken und Speicher sammeln.

---

### **5. AWS Kubernetes Cluster**

Der Studierende kann die grundlegenden Konzepte des Amazon Elastic Kubernetes Service (EKS) verstehen und einen einfachen Kubernetes-Cluster auf AWS bereitstellen.

**Typische Handlungssituation:**  
Ein Unternehmen möchte AWS als Plattform für Kubernetes verwenden. Der Studierende wird beauftragt, einen **EKS**-Cluster auf AWS zu erstellen, einfache Anwendungen bereitzustellen und grundlegende AWS-Dienste wie **Elastic Load Balancing** zu integrieren.

---

### **6. Google Kubernetes Cluster**

Der Studierende versteht die grundlegenden Funktionen von Google Kubernetes Engine (GKE) und kann einfache Kubernetes-Cluster auf Google Cloud erstellen.

**Typische Handlungssituation:**  
Ein Unternehmen plant, Kubernetes-Workloads auf Google Cloud Platform (GCP) zu betreiben. Der Studierende wird beauftragt, einen einfachen **GKE**-Cluster zu erstellen und grundlegende Konfigurationen vorzunehmen, um erste Anwendungen bereitzustellen und die Plattform kennenzulernen.

---

### **7. ClusterAPI**

Der Studierende kann ClusterAPI verwenden, um Kubernetes-Cluster auf mehreren Infrastrukturen automatisiert bereitzustellen, zu verwalten und zu skalieren.

**Typische Handlungssituation:**  

Ein Unternehmen möchte Kubernetes-Cluster automatisiert bereitstellen. Der Studierende wird beauftragt, die Grundlagen von **ClusterAPI** zu erlernen und einen einfachen Cluster mit ClusterAPI auf einer Plattform wie AWS oder Azure zu erstellen, um erste Erfahrungen mit der automatisierten Bereitstellung und Verwaltung von Clustern zu sammeln.
