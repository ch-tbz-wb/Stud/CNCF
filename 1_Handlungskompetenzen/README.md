# Handlungskompetenzen

## Kompetenzmatrix

| **Kompetenzenband**                             | **RP**                                                                               | **HZ**  | **Novizenkompetenz**                                                                 | **Fortgeschrittene Kompetenz**                                                        | **Kompetenz professionellen Handelns**                                                  | **Kompetenzexpertise**                                                                       |
|---------------------------------------|--------------------------------------------------------------------------------------|---------|-------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| **a) Kubernetes in the Enterprise**      | B7.1, B7.4, B12.1, B14.1, C2.1                                                                   |   1      | Grundlegendes Verständnis von Kubernetes in Unternehmensumgebungen und erste Implementierungen. | Implementierung von Kubernetes in einer Unternehmensumgebung mit grundlegender Skalierung und Sicherheit. | Verwaltung von Kubernetes-Clustern mit optimierter Skalierung und Sicherheitsrichtlinien. | Design und strategische Integration von Kubernetes in komplexe IT-Umgebungen eines Unternehmens. |
| **b) OpenShift**                         | B4.6, C2.2, C2.3                                                                     |    2     | Grundlegendes Verständnis von OpenShift und Bereitstellung einfacher Anwendungen.    | Nutzung von OpenShift zur Verwaltung von Kubernetes-Clustern und Anwendungen mit ersten DevOps-Tools. | Verwaltung komplexer Anwendungen und Integration von DevOps-Pipelines und Security-Features in OpenShift. | Design und Verwaltung unternehmensweiter OpenShift-Umgebungen mit fortgeschrittener Automatisierung und Sicherheitsrichtlinien.        |
| **c) VMware Tanzu**                      | B12.1, B12.4, C2.4                                                                   |    3     | Verständnis der Grundkonzepte von VMware Tanzu und einfache Bereitstellung eines Clusters. | Verwaltung von Kubernetes-Clustern auf VMware Tanzu mit ersten Konfigurationen und Skalierungen. | Optimierte Verwaltung und Integration von Kubernetes-Clustern in bestehende VMware-Infrastrukturen. | Design und Automatisierung von VMware Tanzu-Umgebungen für skalierbare und sichere Kubernetes-Cluster im Unternehmenskontext.           |
| **d) Azure Kubernetes Cluster**          | B12.3, C2.3                                                                          |     4    | Erstellung eines einfachen AKS-Clusters und Bereitstellung grundlegender Anwendungen. | Integration und Verwaltung von Kubernetes-Workloads mit Azure-Ressourcen und Sicherheitsrichtlinien. | Skalierung und Verwaltung komplexer AKS-Cluster in einer Cloud-nativen Umgebung.        | Design und Verwaltung grossflächiger AKS-Cluster mit fortschrittlicher Skalierung und Sicherheit in einer Azure-Cloud-Umgebung.         |
| **e) AWS Kubernetes Cluster**            | B14.2, C2.4                                                                          |    5     | Erstellung eines einfachen EKS-Clusters und Integration grundlegender AWS-Dienste.   | Verwaltung von Kubernetes-Clustern mit AWS-Diensten wie ELB und IAM.                  | Skalierung und Optimierung von EKS-Clustern mit fortgeschrittener Netzwerkintegration und Sicherheitsrichtlinien. | Design und Verwaltung von EKS-Clustern auf Unternehmensebene mit vollständiger Integration in die AWS-Infrastruktur.                  |
| **f) Google Kubernetes Cluster**         | B14.1, C2.4                                                                          |    6     | Erstellung eines einfachen GKE-Clusters und Bereitstellung erster Anwendungen.       | Verwaltung von Kubernetes-Workloads auf GKE mit grundlegender Netzwerkintegration und Sicherheitsrichtlinien. | Skalierung und Optimierung von GKE-Clustern für Cloud-native Workloads.                 | Design und Verwaltung grossflächiger GKE-Cluster mit fortschrittlicher Skalierung, Netzwerkintegration und Sicherheit.                  |
| **g) ClusterAPI**                        | B7.1, B7.4, B12.4, B14.1, C2.3                                                                   |    7     | Grundlegendes Verständnis von ClusterAPI und erste Implementierung eines Clusters auf einer Plattform. | Automatisierte Bereitstellung von Kubernetes-Clustern auf verschiedenen Plattformen mit ClusterAPI. | Verwaltung und Optimierung von ClusterAPI für das Cluster-Lifecycle-Management über mehrere Plattformen hinweg. | Design und Automatisierung komplexer Cluster-Management-Strategien mit ClusterAPI für Cloud- und On-Premise-Infrastrukturen.           |

* RP = Rahmenlehrplan
* HZ = Handlungsziele

---

## Modulspezifische Handlungskompetenzen
 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
   
 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen (Niveau: 4)
   * B7.4 Den Einsatz von geschäftsrelevanten Systemen konzipieren (Niveau: 3)   

 * B12 System- und Netzwerkarchitektur bestimmen
   * B12.1 Die bestehende Systemarchitektur beurteilen und weiterentwickeln (Niveau: 3)
   * B12.3 Bestehende ICT Konfigurationen analysieren, Umsetzungsvarianten für die Erweiterung definieren und Soll-Konfigurationen entwickeln (Niveau: 3)
   * B12.4 Die Anforderungen an ein Konfigurationsmanagementsystem einer ICT-Organisation erheben und mögliche Lösungsvarianten vorschlagen (Niveau: 3)   

 * B14 Konzepte und Services umsetzen
   * B14.1 Technische und organisatorische Massnahmen planen und für die Einführung von Software bzw. Releases ausarbeiten (Niveau: 3)
   * B14.2 Probleme und Fehler im operativen Betrieb überwachen, identifizieren, zuordnen, beheben oder falls erforderlich eskalieren (Niveau: 3)

* C2 Cloud-native 
  * C2.1 Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit Container einzusetzen (Niveau: 3)
  * C2.2. Kennt Container Umgebungen (Niveau: 3)
  * C2.3. Jeder Teilnehmer kann eine Container Umgebung aufsetzen (Niveau: 3)
  * C2.4. Jeder Teilnehmer kann eine Container Cluster (Kubernetes) Umgebung aufsetzen (Niveau: 3)

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
