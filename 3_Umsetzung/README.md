# Umsetzung

 - Bereich: Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen, Cloud-native
 - Semester: 5

## Lektionen

* Präsenzlektionen (Vor Ort): 80
* Präsenzlektionen (Distance Learning): 0
* Selbststudium: 20

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                             | **Kompetenzenband**   | **Tools**                                      |
|---------------|-------------------|----------------------------------------|-----------------------|------------------------------------------------|
|  8            | 5                 | Kubernetes in the Enterprise           | a)                    | Kubernetes, Helm, Prometheus, Grafana          |
| 18            | 5                 | OpenShift                              | b)                    | OpenShift, Jenkins, Red Hat OpenShift Console  |
|  9            | 5                 | VMware Tanzu                           | c)                    | VMware Tanzu, vSphere, NSX-T                   |
|  9            | 5                 | Azure Kubernetes Cluster               | d)                    | Azure Kubernetes Service (AKS), Azure DevOps   |
|  9            | 5                 | AWS Kubernetes Cluster                 | e)                    | Amazon EKS, AWS IAM, AWS ELB                   |
|  9            | 5                 | Google Kubernetes Cluster              | f)                    | Google Kubernetes Engine (GKE), Google Cloud   |
|  9            | 5                 | SuSe Rancher                           | f)                    | SuSe Rancher                                   |
|  9            | 5                 | ClusterAPI                             | g)                    | ClusterAPI, Kubernetes                         |
| **80**        | **40**            | **Total**                              |                       |                                                |

## Voraussetzungen

* Microservices (MSVC)
* NoSQL (NoSQL)
* Cloud-native Core (CNC)

## Dispensation 

* Keine

## Methoden

Praktische Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

Container, Kubernetes, Microservices, OpenShift, VMWare Tanzu, Azure Kubernetes Cluster, AWS Kubernetes Cluster

## Lerninhalte

 - Versteht die Anforderungen und Herausforderungen beim Einsatz von Cloud-native Technologien in Enterprise-Umgebungen, einschliesslich Skalierbarkeit, Sicherheit, Compliance und Integration mit bestehenden Systemen.
 - Kann Strategien für die Implementierung und Verwaltung von containerisierten Anwendungen in grossen Unternehmensstrukturen entwickeln, mit einem Fokus auf Automatisierung, Continuous Integration/Continuous Deployment (CI/CD) und DevOps-Best Practices.
 - Ist fähig, komplexe Cloud-native Architekturen zu entwerfen, die hochverfügbar, sicher und kosteneffizient sind, unter Berücksichtigung von Multi-Cloud und Hybrid-Cloud Szenarien.
 - Versteht, wie man die Governance, das Monitoring und die Performance-Optimierung von Cloud-native Anwendungen in einer Unternehmensumgebung sicherstellt, einschliesslich der Anwendung von Policy-Management und Compliance-Überprüfungen.
 - Kann die Besonderheiten und Einsatzbereiche von Cloud-native Plattformen und Technologien wie OpenShift, VMware Tanzu, Azure Kubernetes Service (AKS) und Amazon Elastic Kubernetes Service (EKS) bewerten und vergleichen, um basierend auf den spezifischen Anforderungen und Zielen eines Unternehmens die passendste Technologie auszuwählen und einzusetzen.

## Übungen und Praxis

* Kubernetes in the Enterprise
* OpenShift
* VMWare Tanzu
* Azure Kubernetes Cluster
* AWS Kubernetes Cluster
* Google Kubernetes Engine (GKE)
* ClusterAPI

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Diverse Kubernetes Cluster Umgebungen